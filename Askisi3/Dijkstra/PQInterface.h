/* this is the file "PQInterface.h"      */
#ifndef PQUEUE
#define PQUEUE

#include "PQTypes.h"
/* defines types PQItem and PriorityQueue */

void Initialize (PriorityQueue *);
int Empty (PriorityQueue *);
int Full (PriorityQueue *);
void Insert (PQItem, PriorityQueue *);
PQItem Remove (PriorityQueue *);
void updatePriority(int,int,PriorityQueue*);
void heapify(PriorityQueue **PQ, int i);
void swap(PQItem *i, PQItem *j);
void print(PriorityQueue *Q);

#endif
