/* This code is from the book "Data Structures and Program Design in C" by
R. Kruse, C.L. Tondo and B. Leung
Prentice Hall, 1997. */

/* This is the file BinarySearchTreeTypes.h */

typedef int TreeEntry;
typedef int KeyType;
/* the types TreeEntry and KeyType depend on the application */

typedef struct treenode TreeNode;
typedef struct treenode {
    TreeEntry entry;
    TreeNode *left;
    TreeNode *right;
} TreeNode;
