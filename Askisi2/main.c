#include <stdio.h>
#include <stdlib.h>
#include "BinarySearchTreeInterface.h"

/* This main program is for testing the code
from the book "Data Structures and Program Design in C" by
R. Kruse, C.L. Tondo and B. Leung
Prentice Hall, 1997. */


int main(void)
{

   TreeNode *p, *r, *s;
   int v;


   CreateTree(&r);
    // use the modified InsertTree() to insert the new nodes

   /* Populate the tree with the integers 15, 10, 17, 8, 13, 16 given as input */
   /* while (scanf("%d", &v) != EOF)
    {
        p=(TreeNode *)malloc(sizeof(TreeNode));
        p->entry=v;
        r=InsertTree(r, p);
    } */

   p=(TreeNode *)malloc(sizeof(TreeNode));
   p->entry=15;
   r=InsertTree(r, p);
   p=(TreeNode *)malloc(sizeof(TreeNode));
   p->entry=10;
   r=InsertTree(r, p);
   p=(TreeNode *)malloc(sizeof(TreeNode));
   p->entry=18;
   r=InsertTree(r, p);
   p=(TreeNode *)malloc(sizeof(TreeNode));
   p->entry=8;
   r=InsertTree(r, p);
   p=(TreeNode *)malloc(sizeof(TreeNode));
   p->entry=13;
   r=InsertTree(r, p);
   p=(TreeNode *)malloc(sizeof(TreeNode));
   p->entry=16;
   r=InsertTree(r, p);
   p=(TreeNode *)malloc(sizeof(TreeNode));
   p->entry=17;
   r=InsertTree(r, p);
   p=(TreeNode *)malloc(sizeof(TreeNode));
   p->entry=9;
   r=InsertTree(r, p);
   p=(TreeNode *)malloc(sizeof(TreeNode));
   p->entry=21;
   r=InsertTree(r, p);
   p=(TreeNode *)malloc(sizeof(TreeNode));
   p->entry=20;
   r=InsertTree(r, p);
   p=(TreeNode *)malloc(sizeof(TreeNode));
   p->entry=23;
   r=InsertTree(r, p);

   printf("Nodes of the tree in preorder:\n");
   Preorder(r);

   // test the modified Deletion function
   DeleteKeyTree(&r,15);
   printf("Nodes of the tree in inorder:\n");
   Preorder(r);

   printf("The maximum depth of the tree is: %d\n", MaxDepth(r));

   //find a path with sum (number given by the user)
   int sum;

   printf("Give a possible sum of nodes for a tree's path: ");
   scanf("%d", &sum);
   printf("result : %d \n",HasPathSum(r,sum));

   /* Test whether the tree was constructed correctly by printing
   its nodes in the three orders */
   printf("Nodes of the tree in preorder:\n");
   Preorder(r);

   printf("Nodes of the tree in inorder:\n");
   Inorder(r);

   printf("Nodes of the tree in postorder:\n");
   Postorder(r);

   /* Test the function TreeSearch */
   s=TreeSearch(r, 16);
   printf("The key %d was found\n", s->entry);

   printf("Nodes of the tree in preorder after searching for 16:\n");
   Preorder(r);

   /* Test the function DeleteKeyTree by removing a key e.g., 15 */
   DeleteKeyTree(&r, 15);

   printf("Nodes of the tree in preorder after deleting 15:\n");
   Preorder(r);

   /* Test the function DeleteKeyTree by removing a key e.g., 17 */
   DeleteKeyTree(&r, 17);

   printf("Nodes of the tree in preorder after deleting 17:\n");
   Preorder(r);

   /* Test the function DeleteKeyTree by trying to remove a key that
   does not exist in the tree e.g., 20 */
   DeleteKeyTree(&r, 20);
   printf("Nodes of the tree in preorder after deleting 20:\n");
   Preorder(r);
   ClearTree(&r); //test of the function that clears all the nodes of the binary search tree

	
}
