/* This code is from the book "Data Structures and Program Design in C" by
R. Kruse, C.L. Tondo and B. Leung
Prentice Hall, 1997. */

/* This is the file BinarySearchTreeInterface.h */
#include "BinarySearchTreeTypes.h"
#include "StackInterface.h"

void CreateTree(TreeNode **);
void Preorder(TreeNode *);
void Inorder(TreeNode *);
void Postorder(TreeNode *);
TreeNode *TreeSearch(TreeNode *, KeyType);
TreeNode *InsertTree(TreeNode *, TreeNode *);
void DeleteNodeTree(TreeNode **parentLink);
void DeleteKeyTree(TreeNode **, KeyType);

void ClearTree(TreeNode **);
int MaxDepth(TreeNode *);
int HasPathSum(TreeNode *, int);
