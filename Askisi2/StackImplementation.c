/* This is the file StackImplementation.c */

#include <stdio.h>
#include <stdlib.h>
#include "StackInterface.h"

void InitializeStack(Stack *S)
{
   S->ItemList=NULL;
}

int Empty(Stack *S)
{
   return (S->ItemList==NULL);
}

int Full(Stack *S)
{
  return 0;
}
/* We assume an already constructed stack is not full since it can potentially */
/* grow as a linked structure  */

void Push(TreeNode *X, Stack *S)
{
   StackNode *Temp;

   Temp=(StackNode *) malloc(sizeof(StackNode));

   if (Temp==NULL){
      printf("system storage is exhausted");
   } else {
      Temp->Link=S->ItemList;
      Temp->Item=X;
      S->ItemList=Temp;
   }
}


void Pop(Stack *S, TreeNode **X)
{
   StackNode *Temp;

   if (S->ItemList==NULL){
     printf("attempt to pop the empty stack");
   } else {
      Temp=S->ItemList; // set Temp to top of stack
      *X=Temp->Item;     // set X to the entry of the top of stack
      S->ItemList=Temp->Link; // set top of stack to element below top
   }
}
