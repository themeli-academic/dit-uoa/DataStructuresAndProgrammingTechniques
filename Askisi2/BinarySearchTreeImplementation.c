#include <stdio.h>
#include <stdlib.h>
#include "BinarySearchTreeInterface.h"

/* This code is from the book "Data Structures and Program Design in C" by
R. Kruse, C.L. Tondo and B. Leung
Prentice Hall, 1997. */

/* CreateTree: create a tree.
   Pre: None.
   Post: An empty binary search tree has been created to which root points. */
void CreateTree(TreeNode **root)
{
    *root=NULL;
}

/* Preorder: print each node of the tree in preorder.
   Pre: The binary tree to which root points has been created.
   Post: Each node of the tree has been printed in preorder. */
void Preorder(TreeNode *root)
{
   if (root){
      printf("%d\n", root->entry);
      Preorder(root->left);
      Preorder(root->right);
   }
}

/* Inorder: print each node of the tree in inorder.
   Pre: The binary tree to which root points has been created.
   Post: Each node of the tree has been printed in inorder. */
void Inorder(TreeNode *root)
{
    if(root == NULL) return ;
    Stack *S;
    S = (Stack*) malloc(sizeof(Stack));
    InitializeStack(S);
    TreeNode *CurrentNode = root;
    int done = 0;

    while (!done)
    {
        //printf("search the left most treenode\n");
        /* Reach the left most TreeNode of the current tNode */
        if(CurrentNode !=  NULL)
        {
          /* place pointer to a tree node on the stack before traversing
            the node's left subtree */
             //printf("place pointer to a tree node on the stack before traversing the node;s left subtree\n");
             //printf("Traversing left subtree %d\n",CurrentNode->entry);
             Push(CurrentNode, S);
             CurrentNode = CurrentNode->left;
        }

        /* backtrack from the empty subtree and visit the TreeNode
           at the top of the stack; however, if the stack is empty,
          you are done */
        else
        {
            //printf("reached last left child .\n");
             if (!Empty(S))
             {
                 //printf("Not empty stack and we pop the first element\n");


                 Pop(S,&CurrentNode); printf("%d\n",CurrentNode->entry);
                 //printf("root of subtree : %d. Switch to right subtree.\n", CurrentNode->entry);

                /* we have visited the node and its left subtree.
                  Now, it's right subtree's turn */
                 CurrentNode = CurrentNode->right;
                 //printf("traversing right subtree of, of root %d.\n", CurrentNode->entry);

                 continue;
              }
              else
             {
                 done = 1;
             }
        }

    } /* end of while */
}

/* Postorder: print each node of the tree in postorder.
   Pre: The binary tree to which root points has been created.
   Post: Each node of the tree has been printed in postorder. */
void Postorder(TreeNode *root)
{
    if(root == NULL) return ;

    Stack *S;
    S = (Stack*) malloc(sizeof(Stack));
    InitializeStack(S);

    TreeNode *CurrentNode = root;
    TreeNode *PreviousNode = NULL;
    while(1)
    {
        //reach the leftmost node, pushing right childs and roots
        while(CurrentNode)
        {
            if(CurrentNode->right != NULL)
                Push(CurrentNode->right,S);
            Push(CurrentNode,S);
            CurrentNode = CurrentNode->left;
        }
        // when there, pop the node
        Pop(S,&CurrentNode);
        // if it has a right child
        if(CurrentNode->right != NULL)
        {
            // peek the stack
            TreeNode * temp=NULL;
             if(!Empty(S))
             {
                Pop(S,&temp);
                Push(temp,S);
             }
            // if the right child is in the stack
            if(CurrentNode->right == temp)
            {
                //  remove it
                Pop(S,&temp);
                // push the root before the right child
                Push(CurrentNode,S);
                // then push the right child and repeat the
                // traversal to its leftmost node
                CurrentNode = CurrentNode->right;
            }
            else
            {
                // if the right child has been processed, print the root
                printf("%d \n",CurrentNode->entry);
                CurrentNode = NULL;
            }
        }
        else
        {
            // if there is no right child. print the root
            printf("%d \n",CurrentNode->entry);
            CurrentNode = NULL;
        }
        if(Empty(S)) break;
    }
    // to force print
    fflush(stdout);

}

/* TreeSearch: search for target starting at node root.
   Pre: The tree to which root points has been created.
   Post: The function returns a pointer to a tree node that matches
   target or NULL if the target is not in the tree */
TreeNode *TreeSearch(TreeNode *root, KeyType target)
{
   if (root)
     if (target < root->entry)
       root=TreeSearch(root->left, target);
     else if (target > root->entry)
       root=TreeSearch(root->right, target);
   return root;
}

/* InsertTree: insert a new node in the tree.
   Pre: The binary search tree to which root points has been created.
   The parameter newnode points to a node that has been created and contains a key in its entry.
   Post: The node newnode has been inserted into the tree in such a way that the
   properties of a binary search tree are preserved. */
TreeNode *InsertTree(TreeNode *root, TreeNode *newnode)
{
   TreeNode *CurrentNode = root; //we need to pointers while running the tree, a CurrentNode pointer and a PreviousNode pointer
   TreeNode *PreviousNode = NULL; //at first CurrentNode points at the root of the tree and PreviousNode is NULL
   int right; //I use this variable as a boolean so as to know if the newnode will be the left or the right child

   while (CurrentNode) //Loop until CurrentNode becomes NULL
   {
       right= 0; //every time I reenter the loop I change this variable so as to know its value the last time the loop will be entered
       if (newnode->entry < CurrentNode->entry) //if the newnode we want to add to the tree is less than the current node we go left
       {
            PreviousNode = CurrentNode; //and so the previous node points where current nodes poined
            CurrentNode = CurrentNode->left; //and current nodes now points to the left child
       }
       else //otherwise we go right and do as above, but we also change the value of the right variable to 1
       {
            PreviousNode = CurrentNode;
            CurrentNode = CurrentNode->right;
            right = 1;
       }
   }
   //the next two commands will be done in any case
   CurrentNode = newnode; //we add the newnode to the tree
   CurrentNode->left=CurrentNode->right=NULL; //and its children are NULL

   if(!PreviousNode)//if we never enter the while loop, this means that the tree is empty
   {
       root = CurrentNode; //so the newnode will be the root
   }
   else
   {
       if (right) //if the variable right is 1 the newnode will be the right child
            PreviousNode->right = CurrentNode;
       else
            PreviousNode->left = CurrentNode; //otherwise the left child
   }

   return root;
}

/* DeleteNodeTree: delete a new node from the tree.
   Pre: The parameter p is the address of an actual link (not a copy)
   in a binary search tree, and p is not NULL.
   Post: The node p has been deleted from the binary search tree and the resulting
   smaller tree has the properties required of a binary search tree. */
void DeleteNodeTree(TreeNode **parentLink)
{
   TreeNode *r=*parentLink;   /* used to find place for left subtree */

   if (r==NULL)
     printf("Attempt to delete a nonexistent node from binary search tree\n");
   else if (r->right==NULL)
   {
     *parentLink=r->left;  /* Reattach left subtree */
     free(r);
   }
   else if (r->left==NULL)
   {
     *parentLink=r->right;  /* Reattach right subtree */
     free(r);
   }
   else
   {/* there are two possibilities: we go right and then if there is a left child and it has a right child, this right child will become the left child of its parent
      otherwise if we just go right the right child will remain right to its parent. */

     TreeNode *q, *PreviousNode;
     PreviousNode = *parentLink;
     q=r->right; // running the subtree with the q pointer. first, we go right
     int childq = 0; //we use this variable so as to see if there is a leftmost child
     while(q->left!= NULL)
     {
        childq =1;
        PreviousNode = q;
        q=q->left;

     }  /* leftmost node of right subtree */

     //if we enter the while loop the left child of the right subtree exists
     if (childq)
     {
        (*parentLink)->entry = q->entry; //copy its value to the node we want to delete
        PreviousNode->left = q->right; //its right child becomes the previous node's left child
     }
     else
     {//if there is no left child, this means that the right child of the node we want to delete will take its place
         (*parentLink)->entry = q->entry; //and because is greater than it's parent it will remain right child
         PreviousNode->right = q->right;
     }

     free(q);
   }
}


/* DeleteKeyTree: delete a node with key target from the tree.
   Pre: root is the root of a binary search tree with a node containing key equal to target.
   Post: The node with key equal to target has been deleted and returned. The resulting
   tree has the properties required of a binary search tree.
   Uses: DeleteKeyTree recursively, DeleteNodeTree */
void DeleteKeyTree(TreeNode **root, KeyType target)
{

  if (*root==NULL)
    printf("Attempt to delete key %d not present in the binary search tree\n",target);
  else if (target == (*root)->entry){
    DeleteNodeTree(root);
  } else if (target < (*root)->entry)
    DeleteKeyTree(&(*root)->left, target);
  else
    DeleteKeyTree(&(*root)->right, target);
}



int MaxDepth(TreeNode *root)
{
    if(root == NULL)
    {
        return 0;
    }
    else
    {
         /* compute the depth of each subtree */
           int leftDepth = 1 + MaxDepth(root->left);
           int rightDepth = 1 + MaxDepth(root->right);

           /* use the larger one */
           int depth = (leftDepth > rightDepth) ? leftDepth : rightDepth;
           return depth;
    }
}

int HasPathSum(TreeNode *root, int sum)
{

    if(root==NULL) return 0; //if the tree is empty return

    // check if the node is a leaf and if it is, check if sum has the same value with the node's entry
    if(root->left == NULL && root->right == NULL)  return (sum == root->entry);

    int right,left;

    if(root->right != NULL) //if there is a right subtree
    {
        right =  HasPathSum(root->right,sum - root->entry); //recursivly call of the HasPathSum with arguments the right subtree and the sum minus the root's value
    }

    if(root->left != NULL) //do the same for the left subtree

    {
        left =  HasPathSum(root->left,sum - root->entry);
    }

    int ret = (left>right) ? left : right; //if the left variable is greater than the right (it means that the recursive function has returned 1 for the left subree, then
    //the variable ret takes the value of the left variable, otherwise the right's value and the function returns the variable ret
    return ret;
}

void ClearTree(TreeNode **root)
{
    /* Free recursively all the nodes of the binary search tree*/
    if(*root == NULL) return;

    if((*root)->left)
        ClearTree(&((*root)->left));
    if((*root)->right)
        ClearTree( &((*root)->right) );

        printf("Freeing node : %d\n",(*root)->entry);
        free(*root);
        return;

    return;
}
