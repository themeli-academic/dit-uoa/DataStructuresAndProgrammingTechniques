#include "ListTypes.h"
#include "ListInterface.h"

void InsertNewFirstNode(char *A, NodeType **L)
{
    NodeType *N;
    N=(NodeType *)malloc(sizeof(NodeType));
    strcpy(N->Airport, A);
    N->Link = *L;
    *L = N;
}

void InsertNewLastNode(char *A, NodeType **L)
{
      NodeType *N, *P;

      N=(NodeType *)malloc(sizeof(NodeType));
      strcpy(N->Airport, A);
      N->Link=NULL;

      if (*L == NULL) {
         *L=N;
      } else {
         P=*L;
         while (P->Link != NULL) P=P->Link;
         P->Link=N;
      }

}


void PrintList(NodeType *L)
{
      NodeType *N;

      printf("(");
      N=L;
      while(N != NULL) {
         printf("%s", N->Airport);
         N=N->Link;
         if (N!=NULL) printf(",");
      }
      printf(")\n");
}


void DeleteLastNode(NodeType **L)
{
      NodeType *PreviousNode, *CurrentNode;

      if (*L != NULL) {
         if ((*L)->Link == NULL){
            free(*L);
            *L=NULL;
         } else {
            PreviousNode=*L;
            CurrentNode=(*L)->Link;
            while (CurrentNode->Link != NULL){
               PreviousNode=CurrentNode;
               CurrentNode=CurrentNode->Link;
            }
            PreviousNode->Link=NULL;
            free(CurrentNode);
         }
      }
}


void InsertNewSecondNode (NodeType **L)
{
      NodeType *N;
      N=(NodeType *)malloc(sizeof(NodeType));
      strcpy(N->Airport,"BRU");
      N->Link=(*L)->Link;
      (*L)->Link=N;
}



NodeType *ListSearch(char *A, NodeType *L)
{
      NodeType *N;

      N=L;
      while (N != NULL){
        if (strcmp(N->Airport,A)==0){
           return N;
        } else {
           N=N->Link;
        }
      }
      return N;
}

int LengthCounter (NodeType *L)//this is the function from ex1
{
    NodeType *N=L;
    //printf("Node: %s : ", N->Data);
    if (N->Link == NULL)
    {
        //printf("has NULL link!\n");
        return 1;

    }else{
        //printf("has not a NULL link!\n");
        return 1 + LengthCounter(N->Link);
    }

}

NodeType *Select (NodeType **L, int i)//select the item in the position i
{
    NodeType *N;
    N=*L;
    int length = LengthCounter(N);//calculation of the list's length
    if(i<0 || i>=length) return NULL;//if the list is empty the function returns a NULL pointer
    int count = 0;
    while (count!=i)//search for the element i
    {
        count++;//i have a counter to help me find this element
        N=N->Link;//running the list until I find the element I look for
    }
    return N;//return the pointer at the position i
}

void Replace (NodeType **L, int i, char *A)
{
    NodeType *item = Select(L,i);//I use the select function to find the element in the ith place
    //if there is no need to delete and recreate the element of the list
    //we can just copy the new data in the Airport field like below:
    //strcpy(item->Airport, A);

    //otherwise we create a new node and we replace it in the list deleting the ith element
    NodeType *N, *PreviousNode, *CurrentNode;
    N=(NodeType *)malloc(sizeof(NodeType));
    strcpy(N->Airport, A);//we create a new node and we copy the new value

    if (*L == NULL) return;//if the list is empty, the function returns

    if ((*L)->Link == NULL){//if I have only one element
        free(*L);//I release the memory space used for that element
        *L=N;//and now the pointer points at the new element
    }else{//if I have more than one element
        PreviousNode=*L;
        CurrentNode=(*L)->Link;
        while (CurrentNode != item){//I run the list using two pointers, until they take the i and i-1 values accordingly
           PreviousNode=CurrentNode;
           CurrentNode=CurrentNode->Link;
        }
        PreviousNode->Link=N;//the pointer that pointed at the i-1 node, it now points at the new node that we want to add
        N->Link = CurrentNode->Link;//and the link field of the new node points at the same position that points the link field of the pointer CurrentNode
        free(CurrentNode);//I release the memory space which CurrentNode pointed
    }

}
