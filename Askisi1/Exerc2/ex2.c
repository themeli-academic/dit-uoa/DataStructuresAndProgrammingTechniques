#include "ListTypes.h"
#include "ListInterface.h"


int main(void)
{
      NodeType *L;
      NodeType *item;

      L=NULL;//initializing an empty list
      int length = 0;//initializing the length of the list as zero
      InsertNewFirstNode("ATH", &L);//insert a new node at the list
      PrintList(L);

      InsertNewLastNode("DUS", &L);
      InsertNewLastNode("ORD", &L);
      InsertNewLastNode("SAN", &L);
      PrintList(L);

      InsertNewSecondNode(&L);

      PrintList(L);

      DeleteLastNode(&L);
      PrintList(L);

      if (ListSearch("DUS",L) != NULL) {
        printf("DUS is an element of the list\n");
      }
      InsertNewFirstNode("ATH", &L);
      PrintList(L);
      length = LengthCounter(L);//I use the LengthCounter function to calculate the list length
      int i;
      printf("Give an integer between 1 and %d ", length);//I ask from the user to type an integer between 1 and the list length
      scanf("%d", &i); //read the integer inserted by the user

      Replace(&L,i,"FRA");//I use the replace function to replace the ith node with a new element
      PrintList(L);//printing the list

}


                
    
