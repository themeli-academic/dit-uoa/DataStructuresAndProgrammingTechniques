#include <stdio.h>
#include <stdlib.h>
#include "RobotInterface.h"


int main (int argc,char ** argv)
{
    srand(time(NULL));
    int rows, columns, obstaclesPercent;
    rows = 8;
    columns = 8;
    obstaclesPercent = 50;
    //the user either can excecute the program giving as arguments the rows, the columns and the probability of having obstacles or
    //the programm asks him to give them as input
    if (argc == 4)
    {
        rows = atoi(argv[1]);
        columns = atoi(argv[2]);
        obstaclesPercent = atoi(argv[3]);
    }
    else
    {
        printf("Give a number of rows and columns and %% of obstacles:");
        scanf("%d", &rows);
        scanf("%d", &columns);
        scanf("%d", &obstaclesPercent);
    }
    //creation of the Map using the createMap function
    char ** Map = createMap(rows,columns,obstaclesPercent);
    //i create a start state and a goal state randomly
    int rx1 = rand()%rows;
    int ry1 = rand()%columns;
    Map[rx1][ry1] = START;

    int rx2 = rand()%rows;
    int ry2 = rand()%columns;
    Map[rx2][ry2] = GOAL;



    Stack *VisitedPositions=(Stack*)malloc(sizeof(Stack)); InitializeStack(VisitedPositions); //the stack which is used to save visited positions
    Position start, goal, current, next;

    //initialization of the start, goal and current positions
    start.x = rx1;
    start.y = ry1;
    goal.x = rx2;
    goal.y = ry2;
    current.x = start.x;
    current.y = start.y;

    //print the map
    printf("The map is: \n");
    PrintMap(Map, rows, columns);

    //print the start and the goal positions
    printf("\nStart position :"); PrintPosition(start);
    printf("\nGoal position :"); PrintPosition(goal);

    int steps = 0;
    while(goal.x != current.x || goal.y != current.y)//until the goal is found
    {
        printf("\nStep %d\n",++steps); //print who many steps are done until now
        Map[current.x][current.y] = CURRENT; //the current position is marked on the map (c)
        PrintMap(Map, rows, columns); //print the map

        next = findNextPosition(Map,current, rows, columns); //searching for the next position using the findNextPosition function
        if (equals(next, current))//if the function returns with the same value (without having changed it)
        {
            printf("\nno more position available from "); PrintPosition(next); //prints a message that there aren't any new positions found
            if(Empty(VisitedPositions))//if there are no position into the stack
            {
                printf("\nThere is no path!");//we can't find a path so we return the value -1
                return -1;
            }
            else
            {
                Map[current.x][current.y] = VISITED; //marks the actual current position as visited
                Pop(VisitedPositions, &current); //pop the first element of the stack (the last visited position)
                Map[current.x][current.y] = CURRENT; //make this position current
                printf("\nReverting to position "); PrintPosition(current); //print the new current position
            }
        }
        else //the findNextPosition has found a new space position
        {
            printf("\nVisited "); PrintPosition(current); //print the current position
            printf("\n");
            Map[current.x][current.y] = VISITED; //mark the current as visited
            // CARDU
            StackNode * previousTopOfStack = VisitedPositions->ItemList;  // keep the previous stack top
            Push(current, VisitedPositions); //push new position into the stack
            // set the current state to be descended from the old one
            // if the stack was empty, then the (first) position has a null ancestor
            StackNode * currentTopOfStack = VisitedPositions->ItemList;
            currentTopOfStack->pathLink = previousTopOfStack;
            printf("Set position ");
            if(previousTopOfStack==NULL) printf(" null ");
            else PrintPosition(previousTopOfStack->Item);
            printf(" as the path ancestor of "); PrintPosition(currentTopOfStack->Item);
            printf("\n");
            // CARDU

            //and make current the new position found by the function
            current.x = next.x;
            current.y = next.y;
            printf("\nNow going to "); PrintPosition(current); //print the new current position

        }

    }
    //the goal is found

    CreatePath(current, VisitedPositions, Map, rows, columns); //I change the mark on the path using the CreatePath function
    Map[rx1][ry1] = START; //I put again the S kai G on the map
    Map[rx2][ry2] = GOAL;

    printf("The path is:\n");
    PrintMap(Map, rows, columns); //I print the map with the path
    return 0;
}
