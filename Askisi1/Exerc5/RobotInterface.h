#define OBSTACLE '#'
#define START 'S'
#define GOAL 'G'
#define SPACE '.'
#define VISITED '+'
#define CURRENT 'c'
#define PATH 'o'

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include "StackInterface.h"


char **createMap (int rows, int columns, int obstaclesPercent);
void PrintMap(char **Map, int rows, int columns);
Position findNextPosition (char **Map, Position current, int rows, int columns);
int equals(Position a, Position b);
void PrintPosition(Position a);
void PrintMap (char **Map, int rows, int columns);
void CreatePath (Position current, Stack *VisitedPositions, char **Map, int rows, int columns);

