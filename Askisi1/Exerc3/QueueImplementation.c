/* This is the file QueueImplementation.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "QueueInterface.h"

void InitializeQueue(Queue *Q)  // gotta make this a double pointer
{
    Q->stackHelper = NULL;
    Q->stackQueue = NULL;
}

int Empty(Node *S)//check if the Queue is empty
{
   return(S==NULL);
}

//there is no need to check for a full Queue
//int Full(Queue *Q)
//{
//   return(0);
//}
/* We assume an already constructed queue */
/* is not full since it can potentially grow */
/* as a linked structure.                   */

void Insert(ItemType X, Queue *Q)
{
    // either the Insert() or the Remove() will be simple
    // and the other will be complicated

    // insert a node with X in the queue

    printf("Inserting %s to queue\n",X);

    ItemType item;
    while(!Empty(Q->stackQueue))//Pop all the existing element from the stackQueue and push them to the helperStack
    {
        printf("Inserting %s to non empty queue\n",X);
        if(Q->stackQueue == NULL) printf("is null");
        Pop(&(Q->stackQueue),&item);

        Push(&(Q->stackHelper),item);

    }


    // now helper-stack has all the elements in reverse order (LIFO)
    // push new element to queue-stack

    Push(&(Q->stackQueue),X);
    printf("Pushed %s to stackqueue\n",X);
    PrintLinkedList(Q->stackQueue);

    while(!Empty(Q->stackHelper)) // push the elements from the helper-stack to the queue-stack
    {
        printf("Popping from s-helper, pushing to s-queue\n");
        Pop(&(Q->stackHelper),&item);
        Push(&(Q->stackQueue),item);
    }

}
void Remove(Queue *Q, ItemType *X)
{
    // just pop the top element
    Pop(&(Q->stackQueue),X);
}

void Push(Node **S,ItemType X)
{
    // push item type X on the stack S
    // build a new node
   Node *newNode = (Node*)malloc(sizeof(Node));
   PrintLinkedList(*S);


   if (newNode==NULL){
      printf("System storage is exhausted");
   } else {
       newNode->Link=*S;
        strcpy((char*)newNode->Item,(char*)X);
        *S = newNode;
        PrintLinkedList(*S);
   }
}

void Pop(Node **S, ItemType *X)
{
    printf("popping");
   if (*S==NULL){
      printf("Attempted to remove item from an empty queue");
      X = NULL;
   } else {
       Node *Temp = *S;  // point to the top
       strcpy((char*)X,(char*)Temp->Item);    // copy the data value
       (*S)=Temp->Link;  // set stack top to the next element
       free(Temp);              // free the no-longer-used memory
   }
}


void PrintLinkedList(Node * N)
{
    printf("(");

    while(N != NULL) {
       printf("%s", (char*) N->Item);

       N=N->Link;
       if (N!=NULL) printf(",");
    }
    printf(")\n");
}

void PrintQueue(Queue *Q)//function for printing the Queue
{

    PrintLinkedList(Q->stackQueue);


}

