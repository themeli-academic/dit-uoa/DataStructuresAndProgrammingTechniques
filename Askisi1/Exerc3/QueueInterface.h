/* This is the file QueueInterface.h   */

#include "QueueTypes.h"

void InitializeQueue(Queue *Q);
int Empty(Node *);


void Insert(ItemType X, Queue *Q);
void Remove(Queue *Q, ItemType *X);

void Push(Node** S,ItemType X);
void Pop(Node** S,ItemType *X);

void PrintLinkedList(Node * N);

void PrintQueue(Queue *Q);


